import React from 'react'
import { render } from 'react-dom'
import store from './store/store'
//import Grilla from './components/Grilla'
import ButtonClean from './containers/ButtonClean'
import axios from 'axios'
import { url_config } from './actions/act_config'
import { valida_campos } from './actions/funciones'

class Componente extends React.Component {

    constructor(props) {
        super(props)
        this.state = { data_archivo: [] }
        this.componentWillMount = this.componentWillMount.bind(this)
        this.primera_url = this.primera_url.bind(this)
        this.segunda_url = this.segunda_url.bind(this)
        this.render = this.render.bind(this)
        
    }

    componentWillMount() {
        this.primera_url()
    }

    //Obtiene ruta de config en proyecto local (donde se integre)
    primera_url() {
        const url_config_local = url_config()
        axios.get(url_config_local)
            .then((response) => {
                let url_config_adjntos = response.data.config
                this.segunda_url(url_config_adjntos)
            })
            .catch((error) => {
                console.log('ERROR ARCHIVO CONFIG LOCAL')
            })
    }

    //Obtiene ruta del config donde esta el componente
    segunda_url(url_config_adjntos) {
        axios.get(url_config_adjntos)
            .then((response) => {
                let data_axios = response.data
                let valida_data = valida_campos(data_axios)
                this.setState({ data_archivo: valida_data })
            })
            .catch((error) => {
                console.log('ERROR ARCHIVO CONFIG COMPONENTE')
            })
    }

    render() {
        if (this.state.data_archivo.length == 1) {
            let data_config = this.state.data_archivo[0]

            switch (this.props.componente) {
                case 0:
                    return (
                        <ButtonClean
                            contexto={this.props.contexto}  //Contexto atributos de div de entrada
                            contexto_axios={data_config}    //Contexto de archivo "config.json"
                        />
                    )
                    break
            }
        }
        else if (this.state.data_archivo.length == 0) {
            return (<div> </div>)
        }
    }
}

export default Componente