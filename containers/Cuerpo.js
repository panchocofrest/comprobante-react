import React from 'react'
import Asiento from './Asiento'

class Cuerpo extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidMount() {
        setTimeout(function () {
            $('.nbx-scrollbar').scrollbar({ "showArrows": true, "scrollx": "advanced", "scrolly": "advanced" })
        }, 300);
    }

    render() {
        let incremental = this.props.data.items.length + 1
        return (
            <div className='body-comprobante'>
                <div id="initBody" className='cnt-body-comprobante nbx-scrollbar'>
                    {
                        this.props.data.items.map((item, i) => {
                            if (item.Accion !== 'delete') {
                                i++
                                let movimiento = this.props.source.movimientos[item.Tipo]
                                let movimiento_columns = (movimiento === undefined) ? null : movimiento.columns
                                let movimiento_default = (movimiento === undefined) ? null : movimiento.default
                                let movimientos = (item.Tipo !== null) ? item.Movimientos[item.Tipo] : null
                                return (
                                    <Asiento
                                        key={i}
                                        index={i}
                                        item={item}
                                        item_columns={this.props.source.columns}
                                        item_default={this.props.source.default}
                                        movimientos={movimientos}
                                        movimientos_columns={movimiento_columns}
                                        movimientos_default={movimiento_default}
                                        style='registro level-1'
                                        identificador={'registro_' + i}
                                        callbacks={this.props.callbacks} />
                                );
                            }
                        })
                    }

                    <Asiento
                        key={incremental}
                        index={incremental}
                        item={this.props.source.default}
                        item_columns={this.props.source.columns}
                        item_default={this.props.source.default}
                        movimientos={null}
                        movimientos_columns={null}
                        movimientos_default={null}
                        style='registro level-1 placeholder'
                        identificador={'registro_' + (incremental)}
                        callbacks={this.props.callbacks} />

                </div>


            </div>
        )
    }
}

export default Cuerpo;