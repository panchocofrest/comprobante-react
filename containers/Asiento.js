import React from 'react';
import $ from 'jquery';
import Fila from './Fila';
import Movimiento from './Movimiento'

import { connect } from 'react-redux'

class Asiento extends React.Component {

    constructor(props) {
        super(props)
        this.state = { style: this.props.style }
        this.actualizarCssMovimientos = this.actualizarCssMovimientos.bind(this)
        this.mostrarFilaMovimientoPlaceholder = this.mostrarFilaMovimientoPlaceholder.bind(this)
    }

    componentDidUpdate() {
        this.actualizarCssMovimientos(this.props.identificador)
    }

    actualizarCssMovimientos(key) {
        var Label = $('#' + key)
        if (Label.hasClass('activo')) {
            $(Label).find('.level-2').each(function (index, value) {
                if ($(value).css('height') == '0px') {
                    $(value).css({ 'height': 'auto' });
                }
            })
        }
    }

    mostrarFilaMovimientoPlaceholder(posicion) {
        let id = parseInt('2' + String(Math.random()).split('.')[1])
        if (!(this.props.movimientos_default === null) && !(this.props.movimientos_columns === null))
            return (
                <Movimiento
                    ref='filaInternaMovimientoPlaceholder'
                    key={id}
                    index={this.props.index}
                    posicion={posicion}
                    itemId={this.props.item.Id}
                    movimiento={this.props.movimientos_default}
                    columns={this.props.movimientos_columns}
                    style='registro level-2 level-sin-num placeholder'
                    esPlaceholder={true}
                    identificador={this.props.identificador + id}
                    idPadre={this.props.identificador}
                    tipoContraparte={null} />
            )
    }

    render() {
        let posicion = 1
        let esPlaceholder = (this.state.style === 'registro level-1 placeholder') ? true : false
        let debe = (!this.props.item.Debe) ? 0 : this.props.item.Debe
        let haber = (!this.props.item.Haber) ? 0 : this.props.item.Haber
        let esdebito = this.props.item.EsDebito
        let reset = (esdebito == null) ? true : null
        if (this.props.movimientos != null) {
            return (
                <div
                    id={this.props.identificador}
                    data-item={this.props.item.Id}
                    className={this.state.style}
                    data-mov={this.props.item.Tipo}
                    data-idcuenta={this.props.item.CuentaId}
                    data-requierecentrodecosto={(this.props.item.RequiereCentrodeCosto) ? true : null}
                    data-centrocostoid={(this.props.item.RequiereCentrodeCosto) ? this.props.item.CentroCostosId : null}
                    data-requieresucursal={(this.props.item.RequiereSucursal) ? true : null}
                    data-sucursalid={(this.props.item.RequiereSucursal) ? this.props.item.SucursalId : null}
                    data-esdebito={esdebito}
                    data-debe={debe}
                    data-haber={haber}
                    data-reset={reset} >

                    <Fila
                        posicion={posicion}
                        item={this.props.item}
                        columns={this.props.item_columns}
                        item_default={this.props.item_default}
                        identificador={this.props.identificador}
                        esPlaceholder={false}
                        idPadre={this.props.identificador} />

                    {
                        this.props.movimientos.map((movimiento, i) => {
                            if (movimiento.Accion !== 'delete') {
                                i++
                                posicion++
                                let id = (movimiento.Id === 0) ? parseInt('2' + String(Math.random()).split('.')[1]) : movimiento.Id
                                return (
                                    <Movimiento
                                        key={i}
                                        posicion={posicion}
                                        itemId={this.props.item.Id}
                                        movimiento={movimiento}
                                        columns={this.props.movimientos_columns}
                                        style='registro level-2 level-sin-num'
                                        esPlaceholder={false}
                                        identificador={this.props.identificador + id}
                                        idPadre={this.props.identificador}
                                        tipoContraparte={this.props.item["TipoContraparte"]}
                                        callbacks={this.props.callbacks} />
                                )
                            }
                        })
                    }

                    {
                        this.mostrarFilaMovimientoPlaceholder(posicion + 1)
                    }
                </div>
            )
        }
        else {
            let idcuenta = (parseInt(this.props.item.CuentaId) > 0) ? this.props.item.CuentaId : null
            return (
                <div
                    id={this.props.identificador}
                    data-item={this.props.item.Id}
                    className={this.state.style}
                    data-mov={this.props.item.Tipo}
                    data-idcuenta={idcuenta}
                    data-requierecentrodecosto={(this.props.item.RequiereCentrodeCosto) ? true : null}
                    data-centrocostoid={(this.props.item.RequiereCentrodeCosto) ? this.props.item.CentroCostosId : null}
                    data-requieresucursal={(this.props.item.RequiereSucursal) ? true : null}
                    data-sucursalid={(this.props.item.RequiereSucursal) ? this.props.item.SucursalId : null}
                    data-esdebito={esdebito}
                    data-debe={debe}
                    data-haber={haber}
                    data-reset={reset} >

                    <Fila
                        posicion={posicion}
                        item={this.props.item}
                        columns={this.props.item_columns}
                        item_default={this.props.item_default}
                        identificador={this.props.identificador}
                        esPlaceholder={esPlaceholder}
                        idPadre={this.props.identificador} />
                </div>
            )
        }
    }
}

function mapStateToProps(state, props) {
    return {
        data: state.data
    }
}

export default connect(mapStateToProps, {})(Asiento)
