import React from 'react';
import JqxInput from './jqwidgets-react/react_jqxinput.js';
import JqxButton from './jqwidgets-react/react_jqxbuttons.js';
import JqxCalendar from './jqwidgets-react/react_jqxcalendar.js';
import JqxPanel from './jqwidgets-react/react_jqxpanel.js';
import JqxDropDownList from './jqwidgets-react/react_jqxdropdownlist.js';
import JqxDateTimeInput from './jqwidgets-react/react_jqxdatetimeinput.js';

class ControlJqx extends React.Component {

    constructor(props) {
        super(props)
    }

    render() {
        if (this.props.tipoControl === "Autocomplete") {
            return (
                <JqxInput ref='controlJqxWidget'
                    width='100%'
                    height='20px'
                    value={this.props.valor}
                    theme="nubox"
                    placeHolder={this.props.placeholder}
                    dropDownHeight={200} />
            )
        }

        if (this.props.tipoControl === "DateTimeInput") {
            let currentDate = new Date()

            if (this.props.valor != null && this.props.valor.length > 1) {

                let tempDate
                let currentValue = this.props.valor.trim()
                
                if (currentValue.split(new RegExp(' ', 'g')).length > 0) {
                    tempDate = currentValue.split(new RegExp(' ', 'g')).join('/')
                }
                else if (currentValue.split(new RegExp('-', 'g')).length > 0) {
                    tempDate = currentValue.split(new RegExp('-', 'g')).join('/')
                }
                else {
                    tempDate = currentValue
                }

                let date = tempDate.split("/")
                
                let months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec']
                let meses = ['Ene', 'Feb', 'Mar', 'Abr', 'May', 'Jun', 'Jul', 'Ago', 'Sep', 'Oct', 'Nov', 'Dic']

                let hayMes = false;
                // ASIGNAR MONTH DESDE ARRAY MONTHS
                for (let j = 0; j < months.length; j++) {
                    if (date[1] == months[j]) {
                        hayMes = true;
                        date[1] = months.indexOf(months[j]) + 1
                    }
                }

                // SI NO SE PUDO ASIGNAR SE INTENTA DESDE ARRAY MESES
                if (!hayMes) {
                    // ASIGNAR MES DESDE ARRAY MESES
                    for (let j = 0; j < meses.length; j++) {
                        if (date[1] == meses[j]) {
                            date[1] = meses.indexOf(meses[j]) + 1
                        }
                    }
                }

                if (date[1] < 10) {
                    date[1] = '0' + date[1]
                }

                currentDate = new Date(date[2] + '/' + date[1] + '/' + date[0])
            }

            return (
                <JqxDateTimeInput ref='controlJqxWidget'
                    firstDayOfWeek={1}
                    width={120}
                    height={25}
                    theme={'nubox'}
                    value={new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate())}
                    culture={this.props.culture}
                    formatString={this.props.formatString}
                    animationType={'fade'} />
            )
        }

        return (
            <JqxInput ref='controlJqxWidget'
                width='100%' 
                height='24px' 
                value={this.props.valor} 
                theme={'nubox'} 
                placeHolder={this.props.placeholder} 
                maxLength={this.props.maxlength} />
        )
    }
}

export default ControlJqx;