import React from 'react';
import $ from 'jquery';
import Celda from './Celda';

class Movimiento extends React.Component {

    constructor(props) {
        super(props)
        this.updateTabindexMovimiento = this.updateTabindexMovimiento.bind(this)
    }

    renderCelda(column, i) {
        let style = (column.row.class !== null) ? column.row.class : ''
        let width = (column.row.width !== null) ? 'width-' + column.row.width : ''
        let align = (column.row.align !== null) ? column.row.align : ''

        let css = (column.start === true) ? width + '-' + style : style + ' ' + width
        let classname = 'nivel-li ' + css + ' ' + align

        return (
            <Celda
                key={i}
                itemId={this.props.itemId}
                movimientoId={this.props.movimiento.Id}
                style={classname.trim()}
                index={column.position}
                llave={column.key}
                tipoControl={column.tipoControl}
                source={column.source}
                required={column.row.required}
                type={column.row.type}
                format={column.row.format}
                maxlength={column.row.maxlength}
                min={column.row.min}
                max={column.row.max}
                message={column.row.message}
                placeholder={column.row.placeholder}
                esDatoRequerido={column.row.esDatoRequerido}
                culture={column.row.culture}
                formatString={column.row.formatString}
                value={this.props.movimiento[column.key]}
                esDebito={this.props.movimiento.EsDebito}
                movimientoMonto={this.props.movimiento.Monto}
                tipoContraparte={this.props.tipoContraparte}
                identificador={this.props.identificador}
                documentoId={(column.key == "Prefijo") ? this.props.movimiento.DocumentoId : null}
                tabindex="-1"
                dataAction={this.props.movimiento.Accion}
                tipoCuenta={null}
                esPlaceholder={this.props.esPlaceholder}
                idPadre={this.props.idPadre}
                callbacks={this.props.callbacks} />
        )
    }

    componentDidUpdate() {
        this.updateTabindexMovimiento()
    }

    componentDidMount() {
        this.updateTabindexMovimiento()
    }

    updateTabindexMovimiento() {
        let rowMovimiento = $(this.refs.filaInternaMovimiento)
        
        if (rowMovimiento.parent().hasClass('activo')) {

            let idRegistro = rowMovimiento.parent()[0].id
            $('#' + idRegistro).find('.level-2').find('.nivel-li').each(function (i, dat) {

                if (!$(dat).hasClass('borrar')) {
                    $(dat).removeAttr('tabindex')
                    $(dat).attr('tabindex', '0')
                }

            })

        }
    }

    render() {
        return (
            <div ref='filaInternaMovimiento'
                id={this.props.identificador}
                className={this.props.style} >

                <ul className="nivel-ul">
                    {this.props.columns.map((column, i) => this.renderCelda(column, i))}
                </ul>
            </div>
        );
    }
}

export default Movimiento;