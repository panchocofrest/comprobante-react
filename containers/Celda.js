import React from 'react';
import { connect } from 'react-redux'
import ControlJqx from './ControlJqx'

import { update_field, delete_row } from '../actions/act_grilla'
import { next_focus, expanded_row, collapsed_row, collapsed_all, block_celda_monto, formatear_numero, event_fire } from '../actions/funciones'

class Celda extends React.Component {

    constructor(props) {
        super(props)
        this.state = { flagControl: true, controlTemp: false, clean: false }
        this.eventDelete = this.eventDelete.bind(this)
        this.expanded = this.expanded.bind(this)
        this.eventSet = this.eventSet.bind(this)
        this.changeControlVal = this.changeControlVal.bind(this)
        this.cargaControl = this.cargaControl.bind(this)
        this.renderCelda = this.renderCelda.bind(this)
        this.handlerOnblur = this.handlerOnblur.bind(this)
    }

    componentDidMount() {
        let label = $(this.refs.celdaCargaInicial)[0]
        block_celda_monto(this.props.llave, this.props.value, label)
    }

    eventDelete(event) {
        var Label = $(event.currentTarget)

        if (Label.parent().parent().hasClass('level-1')) {
            // ELIMINA ITEM
            this.props.delete_row(this.props.itemId, null)
        }
        else {
            //ELIMINA MOVIMIENTO
            this.props.delete_row(this.props.itemId, this.props.movimientoId)
        }
    }

    expanded(event) {
        let Label = $(event.currentTarget)

        if (Label.hasClass('label-noexpand'))
            return false

        let filaExterna = Label.parent().parent()

        $(filaExterna).find('.level-2').each(function (index, value) {

            if ($(value).css('height') == '0px') {
                // se expande

                // contraer otros activos
                let currentId = $(value).parent()[0].id
                collapsed_all(currentId)

                expanded_row(value)

                if (index === 0) {
                    let nextIdRow = $(value).find('.nivel-li[tabindex=0]')[0].id
                    document.getElementById(nextIdRow).focus()
                }

            }
            else {
                // se contrae
                collapsed_row(value)
            }
        })
    }

    eventSet(event) {
        let that = this
        let source = []
        let dataAdapter
        let controlTemp = this.refs.ControlJqx.refs.controlJqxWidget

        let input = $('#' + controlTemp.componentSelector)
        let padre = input[0].parentElement
        let divPadre = padre.parentElement.parentElement

        let node = document.createElement("SPAN")
        node.setAttribute('class', 'nuboxFont nubox-invalido error')
        node.setAttribute('style', 'display: block;')

        //carga el adapter en caso de ser cuentas
        if (this.props.data_global.data.Cuentas && this.props.source == "cuentas") {
            let tipocontrapartebytipocomprobante = document.getElementById('grillacomprobantereact').getAttribute('tipocontrapartebytipocomprobante')
            let _tipocontrapartebytipocomprobante = (tipocontrapartebytipocomprobante != undefined) ? tipocontrapartebytipocomprobante : 3
            source = {
                localdata: this.props.data_global.data.Cuentas,
                datatype: "json",
                datafields: [
                    { name: 'IdCuenta', type: 'number' },
                    { name: "Codigo", type: 'string' },
                    { name: 'DescripcionCuenta', type: 'string' },
                    { name: "RequiereCentrodeCosto", type: 'bool' },
                    { name: "RequiereSucursal", type: 'bool' },
                    { name: "RequiereConciliacionBancaria", type: 'bool' },
                    { name: "EsAuxiliar", type: 'bool' },
                    { name: "TipoDeCuentaId", type: "number" }
                ]
            };
            dataAdapter = new $.jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    let result = []
                    records.forEach(function (record) {
                        record.Id = record.IdCuenta
                        record.Detalle = record.Codigo + ' - ' + record.DescripcionCuenta
                        record.TipoCuenta = (record.RequiereConciliacionBancaria === true) ? 'ban' : (record.EsAuxiliar === true) ? 'aux' : null
                        record.TipoContraparte = _tipocontrapartebytipocomprobante
                        result.push(record)
                    })
                }
            })
        }
        //carga el adapter en caso de ser clientes
        //if (this.props.data_global.data.Clientes && this.props.source == "clientes") {
        if (JSON.parse(localStorage.getItem('contrapartes')) && this.props.source == "clientes") {

            /// PRIMERA OPCION
            /*
            let contrapartesFiltradas = (parseInt(padre.dataset.tipocontraparte) === 3) ? this.props.data_global.data.Clientes : []
            if (!(contrapartesFiltradas.length > 0)) {
                this.props.data_global.data.Clientes.forEach(function (current, index) {
                    if (parseInt(current.TipoDeContraparteId) == padre.dataset.tipocontraparte || parseInt(current.TipoDeContraparteId) == 3) {
                        contrapartesFiltradas.push(current)
                    }
                })
            }
            */

            /// SEGUNDA OPCION
            let contrapartesFiltradas = (parseInt(padre.dataset.tipocontraparte) === 3) ? JSON.parse(localStorage.getItem('contrapartes')) : []
            if (!(contrapartesFiltradas.length > 0)) {
                JSON.parse(localStorage.getItem('contrapartes')).forEach(function (current, index) {
                    if (parseInt(current.TipoDeContraparteId) == padre.dataset.tipocontraparte || parseInt(current.TipoDeContraparteId) == 3) {
                        contrapartesFiltradas.push(current)
                    }
                })
            }

            /// TERCERA OPCION
            /*
            let contrapartesFiltradas = JSON.parse(localStorage.getItem('contrapartes'))
            */

            source = {
                localdata: contrapartesFiltradas,
                datatype: "json",
                datafields: [
                    { name: 'Id', type: 'number' },
                    { name: 'RazonSocial', type: 'string' },
                    { name: 'RutFormateado', type: 'string' }
                ]
            };
            dataAdapter = new $.jqx.dataAdapter(source, {
                autoBind: true,
                beforeLoadComplete: function (records) {
                    let result = []
                    records.forEach(function (record) {
                        record.Detalle = record.RutFormateado + ' - ' + record.RazonSocial
                        result.push(record)
                    })
                }
            })
        }
        //carga el adapter en caso de ser centro de costos
        if (this.props.data_global.data.CentrosDeCosto && this.props.source == "centrocosto") {
            source = {
                localdata: this.props.data_global.data.CentrosDeCosto,
                datatype: "json",
                datafields: [
                    { name: 'Id', type: 'number' },
                    { name: 'Codigo', type: 'string' },
                    { name: 'Descripcion', type: 'string' }
                ]
            };
            dataAdapter = new $.jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    let result = []
                    records.forEach(function (record) {
                        record.Detalle = record.Codigo + ' - ' + record.Descripcion
                        result.push(record)
                    })
                }
            })
        }
        //carga el adapter en caso de ser sucursal
        if (this.props.data_global.data.Sucursales && this.props.source == "sucursales") {
            source = {
                localdata: this.props.data_global.data.Sucursales,
                datatype: "json",
                datafields: [
                    { name: 'Id', type: 'number' },
                    { name: 'Codigo', type: 'string' },
                    { name: 'Descripcion', type: 'string' }
                ]
            };
            dataAdapter = new $.jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    let result = []
                    records.forEach(function (record) {
                        record.Detalle = record.Codigo + ' - ' + record.Descripcion
                        result.push(record)
                    })
                }
            })
        }
        //carga el adapter en caso de tipoDocumento
        if (this.props.data_global.data.TiposDeDocumento && this.props.source == "tipoDocumento") {
            source = {
                localdata: this.props.data_global.data.TiposDeDocumento,
                datatype: "json",
                datafields: [
                    { name: 'Id', type: 'number' },
                    { name: 'Codigo', type: 'string' },
                    { name: 'Descripcion', type: 'string' },
                    { name: 'Abreviacion', type: 'string' }
                ]
            };
            dataAdapter = new $.jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    let result = []
                    records.forEach(function (record) {
                        record.Detalle = record.Abreviacion + ' - ' + record.Descripcion
                        result.push(record)
                    })
                }
            })
        }
        //carga el adapter en caso de tipoDocumentoBancario
        if (this.props.data_global.data.TiposDeMovimientoBanc && this.props.source == "tipoDocumentoBancario") {
            source = {
                localdata: this.props.data_global.data.TiposDeMovimientoBanc,
                datatype: "json",
                datafields: [
                    { name: 'Id', type: 'number' },
                    { name: 'Codigo', type: 'string' },
                    { name: 'Descripcion', type: 'string' }
                ]
            };
            dataAdapter = new $.jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    let result = []
                    records.forEach(function (record) {
                        record.Detalle = record.Codigo + ' - ' + record.Descripcion
                        result.push(record)
                    })
                }
            })
        }
        //carga el adapter de prefijo
        if (this.props.data_global.data.Prefijos && this.props.source == "prefijos") {
            source = {
                localdata: this.props.data_global.data.Prefijos,
                datatype: "json",
                datafields: [
                    { name: 'Id', type: 'number' },
                    { name: 'Codigo', type: 'string' }
                ]
            }
            dataAdapter = new $.jqx.dataAdapter(source, {
                beforeLoadComplete: function (records) {
                    let result = []
                    records.forEach(function (record) {
                        record.Detalle = record.Codigo
                        result.push(record)
                    })
                }
            })
        }

        if (!this.state.flagControl) {

            if (this.props.tipoControl == "Autocomplete") {

                $(input[0]).keydown(function (event) {
                    if (event.which == 106) {
                        $('#dropDownButtonContentdropDownComprobantes').focus();
                    }
                })

                controlTemp.selectAll()

                input.jqxInput({ source: dataAdapter, displayMember: "Detalle", valueMember: "Id" })

                input.addClass('base-line')
                input.attr('data-parentid', input.parent()[0].id)
                input.attr('data-source', input.parent().data('tipo'))

                input[0].addEventListener("blur", function (e) {
                    e.stopPropagation()

                    if (this.dataset.source === 'Cliente') {
                        if (that.props.callbacks && 'validarCliente' in that.props.callbacks && typeof that.props.callbacks.validarCliente === 'function') {
                            that.props.callbacks.validarCliente({
                                elementoid: `#${padre.id}`,
                                contraparte: this.value,
                                /** tipocontraparte: this.dataset.tipocontraparte */
                                tipocontraparte: padre.dataset.tipocontraparte,
                                terminarFlujo: function () {
                                    if (!that.state.flagControl) {
                                        that.setState({ flagControl: true, controlTemp: false })
                                    }
                                }
                            })
                        }
                        return false
                    }

                });

                input[0].setAttribute('onKeyPress', 'return validacion.typeAlphaNumeric(event)')

                controlTemp.on('change', (event) => {
                    if (!this.state.flagControl) {
                        if (this.props.llave == 'Documento') {
                            padre.blur()

                            setTimeout(function () {
                                padre.nextSibling.focus()
                            }, 200)
                        }
                        else {
                            next_focus(event.target, 0)
                        }
                    }
                })

                controlTemp.on('close', (event) => {
                    if (!this.state.flagControl) {
                        if (this.props.required && input[0].value.length <= 0) {
                            input[0].className += ' jqx-validator-error-element jqx-validator-error-element-nubox'
                            input[0].parentElement.appendChild(node)
                            document.getElementById('grillacomprobantereact').setAttribute('data-valid', 'error')

                            if (input[0].parentElement != null && input[0].parentElement.dataset.mensajeerror != undefined) {
                                delete input[0].parentElement.dataset.mensajeerror
                            }

                            return false
                        }

                        /**
                         * if (isNaN(event.currentTarget.value)) {
                         */
                        if (this.props.required && input[0].value.trim() !== event.currentTarget.dataset.label.trim()) {
                            input[0].className += ' jqx-validator-error-element jqx-validator-error-element-nubox'
                            input[0].parentElement.dataset.mensajeerror = this.props.message
                            input[0].parentElement.appendChild(node)
                            document.getElementById('grillacomprobantereact').setAttribute('data-valid', 'error')
                            return false
                        }
                        else {

                            if (input[0].parentElement != null && input[0].parentElement.dataset.mensajeerror != undefined) {
                                delete input[0].parentElement.dataset.mensajeerror
                            }

                        }

                        let dataObject = $.grep(dataAdapter.originaldata, function (element, index) {
                            var data = element.Detalle.trim() === input[0].value.trim()

                            if (data == false) {
                                data = element.Id === parseInt(input[0].value)
                            }

                            return data
                        })
                        /**
                         * else {
                         * dataObject = $.grep(dataAdapter.originaldata, function (element, index) {
                         * return (event.currentTarget.dataset.value != undefined) ? element.Id === parseInt(event.currentTarget.dataset.value) : null
                         * })
                         * }
                         */

                        if (this.props.required && !dataObject.length) {
                            input[0].className += ' jqx-validator-error-element jqx-validator-error-element-nubox'
                            input[0].parentElement.dataset.mensajeerror = this.props.message
                            input[0].parentElement.appendChild(node)
                            document.getElementById('grillacomprobantereact').setAttribute('data-valid', 'error')
                            return false
                        }
                        else {
                            if (input[0].parentElement != null && input[0].parentElement.dataset.mensajeerror != undefined) {
                                delete input[0].parentElement.dataset.mensajeerror
                            }
                        }

                        /** 
                         * setea navegacion para requeridos de la cuenta (centros de costo y sucursales)
                         */
                        if (this.props.source === 'cuentas') {
                            let element = $(event.target)
                            let parentelement = element.data('parentid')
                            document.getElementById(parentelement).dataset.centrocosto = dataObject[0].RequiereCentrodeCosto
                            document.getElementById(parentelement).dataset.sucursal = dataObject[0].RequiereSucursal
                            this.navegationRequire(element)

                            collapsed_row(element)
                        }

                        /**
                         * set navegacion a campo prefijo
                         */
                        // if (this.props.llave == "Documento") {
                        //     padre.nextSibling.dataset.documentofactura = dataObject[0].Id
                        // }

                        this.changeControlVal(dataObject[0])
                    }
                })
            }

            if (this.props.tipoControl == "DateTimeInput") {

                controlTemp.open()
                input.addClass('base-line')
                let esActivo = true

                var dtiActivo = function () {
                    if (esActivo) {
                        setTimeout(function () {
                            $('div[id^="calendarjqxWidget"] div[id^="innerCalendarjqxWidget"] div[id^="ViewinnerCalendarjqxWidget"] > div').css('height', '36px')
                            $('div[id^="calendarjqxWidget"] #calendarContent td[id^="cellsTableViewinnerCalendar"] table[id^="cellTableViewinnerCalendar"]').css('height', '190px')
                            dtiActivo()
                        }, 100);
                    }
                }

                dtiActivo()

                controlTemp.on('close', (event) => {
                    esActivo = false
                    next_focus(input[0])
                    this.changeControlVal(event.target.value)
                })
            }

            if (this.props.tipoControl == "Input") {

                let hayTechaPor = false

                $(input[0]).keydown(function (event) {
                    if (event.which == 13 || event.which == 9) {
                        next_focus(input[0])
                        return false
                    }
                    else if (event.which == 106) {
                        hayTechaPor = true
                        if (!window.comprobanteBloqueado) {
                            $('#dropDownButtonContentdropDownComprobantes').focus();
                        }
                        else {
                            $('#jqxbuttonCancelarComprobanteNuevaGrillaReact').focus();
                        }
                        return false
                    }
                })

                input.addClass('base-line')
                let li = document.activeElement
                if (li.dataset.tipo === 'Glosa' && li.dataset.accion === 'insert') {
                    input.val(document.getElementById('glosaComprobanteNuevaGrillaReact').value)
                }

                controlTemp.selectAll()

                if (this.props.type === 'int') {
                    input[0].style.direction = 'rtl'
                    input[0].style.textAlign = 'right'
                    input[0].setAttribute('onKeyPress', 'return validacion.typeNumeric(event)')
                }
                else
                    input[0].setAttribute('onKeyPress', 'return validacion.typeAlphaNumeric(event)')

                input[0].addEventListener("blur", function (e) {
                    e.stopPropagation()

                    if (that.props.llave == 'Debe' || that.props.llave == 'Haber') {
                        let valor = parseFloat(input[0].value.toString().replace(/\,/g, '.'))
                        let total = (isNaN(valor)) ? 0 : Math.round(valor * 100) / 100
                        let registro = document.getElementById(padre.dataset.idpadre)

                        if (that.props.llave == 'Debe') {

                            if (hayTechaPor) {
                                hayTechaPor = false
                                if (!window.comprobanteBloqueado) {
                                    $('#dropDownButtonContentdropDownComprobantes').focus();
                                }
                                else {
                                    $('#jqxbuttonCancelarComprobanteNuevaGrillaReact').focus();
                                }
                                return false;
                            }

                            if (total > 0) {
                                registro.dataset.esdebito = true
                                delete registro.dataset.reset
                                divPadre.nextSibling.querySelectorAll('.nivel-li[data-required="true"]')[0].focus()
                            }
                            else {
                                if (that.props.movimientoId == null) {
                                    registro.dataset.reset = true
                                    delete registro.dataset.esdebito
                                    padre.nextSibling.setAttribute('tabindex', '0')
                                    padre.nextSibling.focus()
                                }
                                else {
                                    if (!(that.props.movimientoMonto - that.props.value > 0)) {
                                        registro.dataset.reset = true
                                        delete registro.dataset.esdebito
                                    }
                                    else {
                                        if (parseFloat(input[0].value.toString().replace(/\,/g, '.')) > 0) {
                                            divPadre.nextSibling.querySelectorAll('.nivel-li[data-required="true"]')[0].focus()
                                        }
                                    }
                                }
                            }
                        }

                        if (that.props.llave == 'Haber') {
                            if (total > 0) {
                                registro.dataset.esdebito = false
                                delete registro.dataset.reset
                            }
                            else {
                                if (that.props.movimientoId == null) {
                                    registro.dataset.reset = true
                                    delete registro.dataset.esdebito
                                }
                                else {
                                    if (!(that.props.movimientoMonto - that.props.value > 0)) {
                                        registro.dataset.reset = true
                                        delete registro.dataset.esdebito
                                    }
                                }
                            }
                        }
                    }
                })

                controlTemp.on('close', (event) => {
                    if (this.props.type !== 'int') {
                        if (input[0].value.length <= 0) {
                            input[0].className += ' jqx-validator-error-element jqx-validator-error-element-nubox'
                            input[0].parentElement.appendChild(node)
                            document.getElementById('grillacomprobantereact').setAttribute('data-valid', 'error')
                            return false
                        }
                    }
                    else {
                        if (this.props.format == true) {
                            let valor = parseFloat(input[0].value.toString().replace(/\,/g, '.'))
                            let total = (isNaN(valor)) ? 0 : Math.round(valor * 100) / 100

                            if (total > this.props.max) {
                                input[0].className += ' jqx-validator-error-element jqx-validator-error-element-nubox'
                                input[0].parentElement.dataset.mensajeerror = this.props.message
                                input[0].parentElement.appendChild(node)
                                return false
                            }
                            else {
                                if (input[0].parentElement != null && input[0].parentElement.dataset.mensajeerror != undefined) {
                                    delete input[0].parentElement.dataset.mensajeerror
                                }
                            }
                        }
                    }

                    that.changeControlVal(input[0].value)
                })
            }
        }
    }

    changeControlVal(dato) {
        if (dato == null) {
            dato = {
                Detalle: null,
                Id: 0
            }
        }

        let obj = JSON.parse(JSON.stringify({}))
        $.extend(obj, this.props.item_default)

        if (!this.state.flagControl) {
            this.setState({ flagControl: true, controlTemp: false }, function () {
                this.props.update_field(this.props.itemId, this.props.movimientoId, this.props.llave, dato, this.props.type, this.props.tipoControl, this.props.dataAction, this.props.esDatoRequerido, obj)
            })
        }
    }

    cargaControl(event) {
        if (!$(event.target).parent().parent().hasClass('placeholder')) {

            let esdebito = this.props.esDebito

            if (document.getElementById(event.target.dataset.idpadre).dataset.reset != undefined) {
                if (event.target.dataset.tipo == 'Debe' || event.target.dataset.tipo == 'Haber') {
                    if (document.getElementById(event.target.dataset.idpadre).dataset.mov != undefined) {
                        if (this.props.movimientoId != null) {
                            event.target.setAttribute('tabindex', '0')
                        }
                    }
                    else {
                        event.target.setAttribute('tabindex', '0')
                    }
                }
            }
            else {
                if (!esdebito) {
                    if (document.getElementById(event.target.dataset.idpadre).dataset.esdebito != undefined)
                        esdebito = JSON.parse(document.getElementById(event.target.dataset.idpadre).dataset.esdebito)
                }

                if (esdebito != null) {
                    if (esdebito) {
                        if (event.target.dataset.tipo == 'Haber') {
                            event.target.setAttribute('tabindex', '-1')
                        }
                    }
                    else {
                        if (event.target.dataset.tipo == 'Debe') {
                            event.target.setAttribute('tabindex', '-1')
                            event.target.nextSibling.focus()
                            return false
                        }
                    }
                }
            }

            /**
             * validacion para habilitar campo de prefijo
             */
            if (this.props.llave === "Prefijo") {
                let tipodocumentoforprefijo = document.getElementById('grillacomprobantereact').getAttribute('tipodocumentoforprefijo')
                let arraytipodocumento = (tipodocumentoforprefijo != undefined) ? tipodocumentoforprefijo.split('|') : [1]
                if (event.target.dataset.documentofactura != undefined && event.target.dataset.documentofactura != null && arraytipodocumento.indexOf(event.target.dataset.documentofactura) != -1) {
                    if (!(this.props.data_global.data.Prefijos.length > 0)) {
                        event.target.nextSibling.focus()
                        event.target.setAttribute('tabindex', '-1')
                    }
                }
                else {
                    event.target.nextSibling.focus()
                    event.target.setAttribute('tabindex', '-1')
                }
            }

            if (event.target.getAttribute('tabindex') === '-1') {
                return false
            }

            this.setState({ flagControl: false, controlTemp: true }, function () {
                this.eventSet()
            })
        }
    }

    /**
     * Habilita o desabilita la navegacion por las celdas de Centro de costo y Sucursal
     * @param {*} element 
     */
    navegationRequire(element) {
        let input = element
        if (input[0].tagName == 'INPUT' && input.data('source') === 'Cuenta') {
            let parentid = input.data('parentid')

            let requiereCentrodeCosto = document.getElementById(parentid).dataset.centrocosto
            let requiereSucursal = document.getElementById(parentid).dataset.sucursal

            let ul = $('#' + parentid).parent()[0]

            $(ul).find('.nivel-li').each(function (index, element) {
                switch ($(element).data('tipo')) {
                    case 'CentroCostos':
                        if (requiereCentrodeCosto === 'false') {
                            $(element).attr('tabindex', '-1')
                            $(element).html('')
                        }
                        else {
                            $(element).attr('tabindex', '0')
                        }
                        break;
                    case 'Sucursal':
                        if (requiereSucursal === 'false') {
                            $(element).attr('tabindex', '-1')
                            $(element).html('')
                        }
                        else {
                            $(element).attr('tabindex', '0')
                        }
                        break;
                }
            })
        }
    }

    handlerOnblur(event) {

        if (this.props.llave === 'Debe') {
            if (parseInt(document.getElementById(event.target.id).value) > 0) {
                document.getElementById(event.target.id).parentElement.nextSibling.setAttribute('tabindex', '-1')
            }
        }
        else if (this.props.llave === 'Haber') {
            if (parseInt(document.getElementById(event.target.id).value) > 0)
                document.getElementById(event.target.id).parentElement.previousSibling.setAttribute('tabindex', '-1')
        }

    }

    /**
     * Muestra el valor agregado
     * @param {*} value 
     */
    printValue(value) {
        if (this.props.type === 'int' && this.props.format === true && value !== null) {
            if (this.props.esPlaceholder)
                return value

            if (value > 0) {
                return formatear_numero(value)
            }
            else
                return null
        }

        return value
    }

    renderCelda() {
        let tindex = this.props.tabindex
        let value = this.props.value
        let dataTipoContraparte = null

        if (this.props.llave === 'Debe' || this.props.llave === 'Haber') {
            if (value != null) {
                value = this.props.value.toString()//.replace(/\./g, ',')
            }
        }

        // bloquear celdas debe o haber segun tipo cuenta
        if (this.props.movimientoId === null) {
            if (this.props.tipoCuenta === 'aux' || this.props.tipoCuenta === 'ban')
                if (this.props.llave === 'Debe' || this.props.llave === 'Haber')
                    tindex = '-1'

            if (this.props.llave === 'CentroCostos' && !this.props.requiereCentrodeCosto)
                tindex = '-1'

            if (this.props.llave === 'Sucursal' && !this.props.requiereSucursal)
                tindex = '-1'
        }
        else {
            dataTipoContraparte = (this.props.llave === 'Cliente') ? this.props.tipoContraparte : null
        }

        if (this.props.llave === 'Remove') {
            return (
                <li className={this.props.style} onClick={this.eventDelete}></li>
            )
        }

        if (this.props.llave === 'Num') {
            return (
                <li className={this.props.style}>{this.props.posicion}</li>
            )
        }

        if (this.props.llave === 'Tipo') {
            let style = (this.props.value === 'ban' || this.props.value === 'aux') ? 'label collapsed' : 'label-noexpand'
            let css = 'nivel-li ' + this.props.value + ' ' + style
            return (
                <li className={css.trim()} onClick={this.expanded}></li>
            )
        }

        if (this.state.flagControl) {
            return (
                <li
                    ref="celdaCargaInicial"
                    className={this.props.style}
                    data-tipo={this.props.llave}
                    data-accion={this.props.dataAction}
                    data-value={this.props.value}
                    data-required={this.props.required}
                    data-esPlaceholder={this.props.esPlaceholder}
                    data-idPadre={this.props.idPadre}
                    data-tipoContraparte={dataTipoContraparte}
                    data-documentoFactura={this.props.documentoId}
                    role="button"
                    aria-pressed="false"
                    tabIndex={tindex}
                    id={this.props.identificador + '_' + this.props.index}
                    onFocus={this.cargaControl.bind(this)} >

                    {this.printValue(this.props.value)}

                </li>
            )
        }
        else {
            return (
                <li
                    ref="celdaCargaInicial"
                    className={this.props.style}
                    data-tipo={this.props.llave}
                    data-accion={this.props.dataAction}
                    data-value={this.props.value}
                    data-required={this.props.required}
                    data-esPlaceholder={this.props.esPlaceholder}
                    data-idPadre={this.props.idPadre}
                    data-tipoContraparte={dataTipoContraparte}
                    data-documentoFactura={this.props.documentoId}
                    role="button"
                    aria-pressed="false"
                    tabIndex={tindex}
                    id={this.props.identificador + '_' + this.props.index} >

                    <ControlJqx ref='ControlJqx'
                        tipoControl={this.props.tipoControl}
                        valor={value}
                        placeholder={this.props.placeholder}
                        maxlength={this.props.maxlength}
                        culture={this.props.culture}
                        formatString={this.props.formatString} />

                </li>
            )
        }
    }

    render() {
        return (
            this.renderCelda()
        )
    }
}

function mapStateToProps(state, props) {
    return {
        data: state.data,
        data_global: state.data_global,
        data_cuenta: state.data_cuenta,
        data_sucursal: state.data_sucursal,
        data_cliente: state.data_cliente,
        data_tipo_documento: state.data_tipo_documento,
        data_tipo_documento_bancario: state.data_tipo_documento_bancario,
        data_prefijo: state.data_prefijo
    }
}

export default connect(mapStateToProps, { update_field, delete_row })(Celda)