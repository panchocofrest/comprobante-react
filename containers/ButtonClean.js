import React from 'react'
import Grilla from '../components/Grilla'

import { connect } from 'react-redux'
import { clean_data_for_save, set_data_default } from '../actions/act_grilla'
import { collapsed_row } from '../actions/funciones'

class ButtonClean extends React.Component {
    constructor(props) {
        super(props)
        this.state = { clean: false }
        this.enableClean = this.enableClean.bind(this)
        this.SetDataDefault = this.SetDataDefault.bind(this)
        this.collapsedAllRows = this.collapsedAllRows.bind(this)
    }

    enableClean() {
        let that = this
        let noErrors = false
        this.props.clean_data_for_save()

        /**
         * si agrego cuenta en el movimiento contable y esta requiere CentroCosto o Sucursal
         * identificar si completo estos datos requeridos
         */
        /*
        let movimientoConCuenta = document.getElementById('initBody').querySelectorAll('.registro.level-1[data-idcuenta]')
        movimientoConCuenta.forEach(function (div, index) {
            if (div.dataset.requierecentrodecosto != undefined && !(div.dataset.centrocostoid > 0)) {
                noErrors = true
                that.eventFire(document.getElementById(div.querySelectorAll('.nivel-li[data-tipo="CentroCostos"]')[0].id), 'focus')
                that.eventFire(document.getElementById(div.querySelectorAll('.nivel-li[data-tipo="CentroCostos"]')[0].firstChild.id), 'focus')
            }
            if (div.dataset.requieresucursal != undefined && !(div.dataset.sucursalid > 0)) {
                noErrors = true
                that.eventFire(document.getElementById(div.querySelectorAll('.nivel-li[data-tipo="Sucursal"]')[0].id), 'focus')
                that.eventFire(document.getElementById(div.querySelectorAll('.nivel-li[data-tipo="Sucursal"]')[0].firstChild.id), 'focus')
            }
        })
        */
        if (!noErrors) {
            let opcion = JSON.parse(document.getElementById("btonComprobateReactEnableClean").dataset.clean)
            that.setState({ clean: !opcion }, function () {
                that.setState({ clean: opcion })
                document.getElementById("grillacomprobantereact").dataset.valid = "success"
            })
        }
    }

    SetDataDefault() {
        this.collapsedAllRows()
        this.props.set_data_default([this.props.contexto_axios.source.default])
    }

    collapsedAllRows() {
        $('#grid').find('.level-1').each(function (position, level1) {
            if ($(level1).hasClass('activo')) {
                $(level1).find('.level-2').each(function (i, row) {
                    collapsed_row(row)
                })
            }
        })
    }

    render() {
        if (this.state.clean == false) {
            return (
                <div>
                    <input type="hidden" id="btonComprobateReactEnableClean" data-clean={this.state.clean} onClick={this.enableClean} />
                    <input type="hidden" id="btonComprobateReactSetDataDefault" onClick={this.SetDataDefault} />
                    <Grilla
                        contexto={this.props.contexto}
                        contexto_axios={this.props.contexto_axios}
                    />
                </div>
            )
        }
        else {
            return (
                null
            )
        }
    }
}

function mapStateToProps(state, props) {
    return {}
}

export default connect(mapStateToProps, { clean_data_for_save, set_data_default })(ButtonClean)