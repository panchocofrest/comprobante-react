import React from 'react';
import Celda from './Celda';
import { expanded_row } from '../actions/funciones'

class Fila extends React.Component {

    constructor(props) {
        super(props)
    }

    componentDidUpdate() {
        let li = document.activeElement
        if (li.dataset.tipo === 'Glosa' && li.dataset.accion === 'insert') {
            let div = li.parentElement.parentElement

            $(div).find('.level-2').each(function (index, value) {
                if ($(value).css('height') == '0px') {
                    expanded_row(value)
                }
            })
        }
    }

    renderCelda(column, i) {
        let style = (column.row.class !== null) ? column.row.class : ''
        let width = (column.row.width !== null) ? 'width-' + column.row.width : ''
        let align = (column.row.align !== null) ? column.row.align : ''

        let css = (column.start === true) ? width + '-' + style : style + ' ' + width
        let classname = 'nivel-li ' + css + ' ' + align
        let requerido = (this.props.esPlaceholder) ? column.row.required : (column.key == 'CentroCostos') ? this.props.item.RequiereCentrodeCosto : (column.key == 'Sucursal') ? this.props.item.RequiereSucursal : column.row.required

        return (
            <Celda
                key={i}
                itemId={this.props.item.Id}
                movimientoId={null}
                style={classname.trim()}
                index={column.position}
                llave={column.key}
                tipoControl={column.tipoControl}
                source={column.source}
                required={requerido}
                type={column.row.type}
                format={column.row.format}
                maxlength={column.row.maxlength}
                min={column.row.min}
                max={column.row.max}
                message={column.row.message}
                placeholder={column.row.placeholder}
                value={this.props.item[column.key]}
                esDebito={this.props.item["EsDebito"]}
                requiereCentrodeCosto={this.props.item["RequiereCentrodeCosto"]}
                requiereSucursal={this.props.item["RequiereSucursal"]}
                identificador={this.props.identificador}
                tabindex="0"
                dataAction={this.props.item.Accion}
                tipoCuenta={this.props.item['Tipo']}
                posicion={this.props.item.Num}
                esPlaceholder={this.props.esPlaceholder}
                idPadre={this.props.idPadre}
                item_default={this.props.item_default} />
        )
    }

    render() {
        return (
            <ul className="nivel-ul">
                {this.props.columns.map((column, i) => this.renderCelda(column, i))}
            </ul>
        );
    }
}

export default Fila;