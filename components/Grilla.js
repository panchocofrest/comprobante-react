import React from 'react'
import $ from 'jquery'
import Cabecera from '../containers/Cabecera'
import Cuerpo from '../containers/Cuerpo'

import { connect } from 'react-redux'
import { ruteador_cabecera } from '../actions/act_config'
import { get_datos, add_item, add_movimiento } from '../actions/act_grilla'
import { next_focus, expanded_row, collapsed_row, collapsed_all, formatear_numero } from '../actions/funciones'

class Grilla extends React.Component {
  constructor(props) {
    super(props)
    this.state = { idComprobante: 0 }
    this.reloadGrilla = this.reloadGrilla.bind(this)
    this.loadData = this.loadData.bind(this)
    this.collapsedAllRows = this.collapsedAllRows.bind(this)
    this.handlerKeyPress = this.handlerKeyPress.bind(this)
    this.handlerOnFocus = this.handlerOnFocus.bind(this)
    this.getTotalesDebe = this.getTotalesDebe.bind(this)
    this.getTotalesHaber = this.getTotalesHaber.bind(this)
  }

  componentWillMount() {
    let idComprobante = parseInt(document.getElementById('grillacomprobantereact').getAttribute('idcomprobante'))
    this.loadData(idComprobante)
    this.collapsedAllRows()
  }

  componentDidUpdate() {
    let div = document.getElementById('initBody').firstChild

    if ($(div).hasClass('placeholder')) {
      $(div).removeClass('placeholder')

      var obj = JSON.parse(JSON.stringify({}))
      $.extend(obj, this.props.contexto_axios.source.default)
      this.props.add_item(obj)
    }

    if (document.getElementById('grid').querySelectorAll('.level-1.placeholder')[0] !== undefined) {
      document.getElementById('grid').querySelectorAll('.level-1.placeholder')[0].firstChild.firstChild.innerText = ''
    }

    if (document.getElementById('grillacomprobantereact').querySelectorAll('.nubox-invalido').length == 0) {
      document.getElementById('grillacomprobantereact').setAttribute('data-valid', 'success')
    }
  }

  reloadGrilla() {
    let comprobanteid = parseInt(document.getElementById('grillacomprobantereact').getAttribute('idcomprobante'))
    this.setState({ idComprobante: comprobanteid }, function () {
      this.loadData(comprobanteid)
      this.collapsedAllRows()
    })
  }

  loadData(idComprobante) {
    let dev = JSON.parse(document.getElementById('grillacomprobantereact').getAttribute('dev'))
    let token = document.getElementById('grillacomprobantereact').getAttribute('token')
    let sujetoContableId = document.getElementById('grillacomprobantereact').getAttribute('sujetocontableid')
    let ruta = 'http://localhost:8095/dataGrid-nodata.json'
    //let ruta = 'http://localhost:8095/dataGrid.json'
    let method = 'GET'

    if (idComprobante === undefined)
      return false

    if (!dev) {
      ruta = `${window.location.origin}/${window.location.pathname.split('/')[1]}`
      if (ruta.substring(ruta.length - 1, ruta.length) === '/')
        ruta = ruta.substring(0, ruta.length - 1)

      //ruta += this.props.contexto_axios.url_config + '/' + sujetoContableId + '/' + idComprobante
      ruta += `${this.props.contexto_axios.url_config}/${idComprobante}`
      method = 'POST'

      if (idComprobante === 0)
        ruta = null
    }

    let dataDefault = []
    dataDefault.push(this.props.contexto_axios.source.default)

    this.props.get_datos(ruta, ruteador_cabecera(token, method, {}, 'application/json', dev), dataDefault)
  }

  collapsedAllRows() {
    $('#grid').find('.level-1').each(function (position, level1) {
      if ($(level1).hasClass('activo')) {
        $(level1).find('.level-2').each(function (i, row) {
          collapsed_row(row)
        })
      }
    })
  }

  handlerKeyPress(event) {

    let enter = true
    let plus = true

    if (plus) {
      if (event.charCode === 43) {
        let element = event.target.parentElement.parentElement.parentElement

        if (event.target.tagName === 'LI')
          element = event.target.parentElement.parentElement

        if (element.getAttribute('class').split(' ').indexOf('level-2') >= 1)
          element = element.parentElement

        $(element).find('.level-2').each(function (index, value) {
          if ($(value).css('height') == '0px') {
            let currentId = $(value).parent()[0].id
            collapsed_all(currentId)
            expanded_row(value)
            if (index === 0) {
              let nextIdRow = $(value).find('.nivel-li[tabindex=0]')[0].id
              document.getElementById(nextIdRow).focus()
            }
          }
          else {
            collapsed_row(value)
            element.nextSibling.querySelectorAll('.nivel-li[data-tipo="Cuenta"]')[0].focus()
          }
        })
      }
    }

    if (enter) {
      if (event.charCode === 13) {
        //next_focus(event.target)
      }
    }

  }

  handlerOnFocus(event) {
    let that = this

    let elemento = event.target
    let idPadre = elemento.parentElement.parentElement.id

    if (elemento.tagName == 'INPUT') {
      idPadre = elemento.parentElement.id
    }

    let padre = document.getElementById(idPadre)

    if (padre != null && padre.classList.contains('placeholder')) {

      if (JSON.parse(elemento.dataset.required)) {

        if (padre.classList.contains('level-1')) {
          // Fila

          var obj = JSON.parse(JSON.stringify({}))
          $.extend(obj, this.props.contexto_axios.source.default)
          this.props.add_item(obj)
        }
        else {
          // Movimiento

          let id = elemento.dataset.idpadre
          let subId = padre.id
          let esdebito = null

          let containerMovimientos = document.getElementById(id)
          let idMovimiento = subId.substring(id.length, subId.length)

          var obj = JSON.parse(JSON.stringify({}))
          $.extend(obj, this.props.contexto_axios.source.movimientos[containerMovimientos.dataset.mov].default)
          obj.Id = idMovimiento

          if (containerMovimientos.dataset.esdebito != undefined)
            esdebito = JSON.parse(containerMovimientos.dataset.esdebito)

          this.props.add_movimiento(obj, parseInt(containerMovimientos.dataset.item), esdebito)
        }

        setTimeout(function () {
          document.getElementById(elemento.id).focus()
        }, 200)

      }

    }
    else {
      if (elemento.tagName == 'LI') {
        if (elemento.dataset.tipo == 'Cuenta') {
          this.collapsedAllRows()
        }
      }
    }

  }

  getTotalesDebe() {
    let totalDebe = 0
    this.props.data.items.map((item, i) => {
      if (item.Accion != "delete") {
        totalDebe += item.Debe
      }
    })

    return formatear_numero(totalDebe);
  }

  getTotalesHaber() {
    let totalHaber = 0
    this.props.data.items.map((item, i) => {
      if (item.Accion != "delete") {
        totalHaber += item.Haber
      }
    })

    return formatear_numero(totalHaber);
  }

  render() {
    return (
      <div id='grid'>
        <input type="hidden" id="btonComprobateReact" onClick={this.reloadGrilla} />
        <div
          className='cnt-comprobante'
          onKeyPress={this.handlerKeyPress}
          onFocus={this.handlerOnFocus}
        >

          <Cabecera
            columns={this.props.contexto_axios.source.columns} />

          <Cuerpo
            source={this.props.contexto_axios.source}
            data={this.props.data}
            callbacks={this.props.contexto.callbacks} />
        </div>
        <div className="ctn-totalizador">
          <div className="celda totales">$ Totales:</div>
          <div className="celda cifra">{this.getTotalesDebe()}</div>
          <div className="celda cifra">{this.getTotalesHaber()}</div>
          <div className="scroll-fix"></div>
        </div>
      </div>
    )
  }
}

function mapStateToProps(state, props) {
  return {
    data: state.data
  }
}

export default connect(mapStateToProps, { get_datos, add_item, add_movimiento })(Grilla)