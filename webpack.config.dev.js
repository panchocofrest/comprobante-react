require("babel-polyfill");
require('es6-promise').polyfill();
var path        = require('path');
var webpack     = require('webpack');
var ExtractTextPlugin   = require('extract-text-webpack-plugin');
var ROOT_PATH           = path.resolve(__dirname);

module.exports = {
    entry:
    {
        comprobante       : ['es6-promise', 'whatwg-fetch', path.resolve(ROOT_PATH, 'comprobante.jsx')],
    },
    output:
    {
        path        : path.resolve(ROOT_PATH , 'public'),
        filename    : 'reactreduxjqx.[name].js',
        library     : '[name]'
    },
    devServer:
    {
        contentBase     : __dirname + '/public/',
        compress        : true,
        port            : 8095,
        open            : true,
    },
    module:
    {
         loaders: 
        [
            {
                test: /.jsx?$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
                query: 
                {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.json?$/,
                use: 'json-loader'
            },
            { 
                test: /\.css$/,
                use: ExtractTextPlugin.extract({use: 'css-loader?importLoaders=1',}),
            },
            { 
                test: /\.(sass|scss)$/, 
                use: ExtractTextPlugin.extract({
					fallback: 'style-loader',
						use: [
						'css-loader?url=false&minimize=true',
						'sass-loader?sourceMap'
					],                  
				})               
            },
            {   
                test: /\.(ico|jpg|png|gif|eot|otf|webp|svg|ttf|woff|woff2)(\?.*)?$/,
                use: ExtractTextPlugin.extract(
                    {
                        use: 'file-loader',                  
                    }),                
            },
            {
                test: /\.(jpe?g|png|gif|svg)$/,
                use: ExtractTextPlugin.extract(
                    {
                        loader: 'file-loader',
                        options:
                        {
                            name: './img/[name].[ext]'
                        }                    
                    }),    
            },

        ],   
    }
  ,
    plugins: [
        new ExtractTextPlugin({ 
            filename: './assets/css/reactreduxjqx.css',
            allChunks: true,
        }),
        new webpack.DefinePlugin('transform-es2015-classes'),
        new webpack.DefinePlugin('transform-object-assign'),
        new webpack.ProvidePlugin({
            Promise: 'es6-promise',
            React: 'react'
        })
         
    ]
};