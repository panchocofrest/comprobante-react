import { render } from 'react-dom';
import { Provider } from 'react-redux';
import React from 'react';
import store from './store/store'
import axios from 'axios'
import { url_config, ruteador_cabecera } from './actions/act_config'
import Grilla from './axios.jsx'
import $ from 'jquery';
import './scss/build_scss/build_completo.scss'

export const objectAssign = require('object-assign');

export const SET_DATA_CUENTA = 'SET_DATA_CUENTA'
export const SET_DATA_CENTRO_COSTO = 'SET_DATA_CENTRO_COSTO'
export const SET_DATA_SUCURSALES = 'SET_DATA_SUCURSALES'
export const SET_DATA_CLIENTES = 'SET_DATA_CLIENTES'
export const SET_DATA_TIPO_DOCUMENTO = 'SET_DATA_TIPO_DOCUMENTO'
export const SET_DATA_TIPO_DOCUMENTO_BANCARIO = 'SET_DATA_TIPO_DOCUMENTO_BANCARIO'
export const SET_DATA_PREFIJO = 'SET_DATA_PREFIJO'

const cargar_data = (root_index) => {
    return new Promise((resolve, reject) => {
        let respuesta = []
        let dev = JSON.parse(root_index.getAttribute('dev'))
        let token = root_index.getAttribute('token')
        let url_base = root_index.getAttribute('urlbase')
        let controller = root_index.getAttribute('controller')
        let api = 'Api/'

        
        if (!dev) {
            let ruta = window.location.origin + '/' + window.location.pathname.split('/')[1]
            if (ruta.substring(ruta.length - 1, ruta.length) === '/')
                ruta = ruta.substring(0, ruta.length - 1)

            url_base = ruta
            api = ''
        }

        var get_cuentas = new Promise((resolve, reject) => {
            let url = `${url_base}/${controller}/${api}ObtenerCuentasPorSujetoContable`
            let data = {}

            fetch(url, ruteador_cabecera(token, 'POST', data, undefined, dev))
                .then((response) => {
                    return response.json()
                })
                .then((result) => {
                    objectAssign(respuesta, respuesta, result);
                    store.dispatch({ type: 'SET_DATA_CUENTA', data_cuenta: respuesta })
                    resolve()
                })
        })

        var get_centroscosto = new Promise((resolve, reject) => {
            let url = `${url_base}/${controller}/${api}ObtenerCcPorSujetoContable`
            let data = {}

            fetch(url, ruteador_cabecera(token, 'POST', data, undefined, dev))
                .then((response) => {
                    return response.json()
                })
                .then((result) => {
                    objectAssign(respuesta, respuesta, result);
                    store.dispatch({ type: 'SET_DATA_CENTRO_COSTO', data_centrocosto: respuesta })
                    resolve()
                })
        })

        var get_sucursales = new Promise((resolve, reject) => {
            let url = `${url_base}/${controller}/${api}ObtenerSucPorSujetoContable`
            let data = {}

            fetch(url, ruteador_cabecera(token, 'POST', data, undefined, dev))
                .then((response) => {
                    return response.json()
                })
                .then((result) => {
                    objectAssign(respuesta, respuesta, result);
                    store.dispatch({ type: 'SET_DATA_SUCURSALES', data_sucursal: respuesta })
                    resolve()
                })
        })

        var get_clientes = new Promise((resolve, reject) => {
            let url = `${url_base}/${controller}/${api}ObtenerClientesPorSistemaComputacional`
            let data = {}

            fetch(url, ruteador_cabecera(token, 'POST', data, undefined, dev))
                .then((response) => {
                    return response.json()
                })
                .then((result) => {
                    objectAssign(respuesta, respuesta, result);
                    store.dispatch({ type: 'SET_DATA_CLIENTES', data_cliente: respuesta })
                    resolve()
                })
        })

        var get_tiposdocumento = new Promise((resolve, reject) => {
            let url = `${url_base}/${controller}/${api}ObtenerTiposDeDocumento`
            let data = {}

            fetch(url, ruteador_cabecera(token, 'POST', data, undefined, dev))
                .then((response) => {
                    return response.json()
                })
                .then((result) => {
                    objectAssign(respuesta, respuesta, result);
                    store.dispatch({ type: 'SET_DATA_TIPO_DOCUMENTO', data_tipo_documento: respuesta })
                    resolve()
                })
        })

        var get_tipodocumentosBancarios = new Promise((resolve, reject) => {
            let url = `${url_base}/${controller}/${api}ObtenerTiposDeMovimientosBancarios`
            let data = {}

            fetch(url, ruteador_cabecera(token, 'POST', data, undefined, dev))
                .then((response) => {
                    return response.json()
                })
                .then((result) => {
                    objectAssign(respuesta, respuesta, result);
                    store.dispatch({ type: 'SET_DATA_TIPO_DOCUMENTO_BANCARIO', data_tipo_documento_bancario: respuesta })
                    resolve()
                })
        })

        var get_prefijos = new Promise((resolve, reject) => {
            let url = `${url_base}/${controller}/${api}ObtenerPrefijos`
            let data = {}

            fetch(url, ruteador_cabecera(token, 'POST', data, undefined, dev))
                .then((response) => {
                    return response.json()
                })
                .then((result) => {
                    objectAssign(respuesta, respuesta, result);
                    store.dispatch({ type: 'SET_DATA_PREFIJO', data_prefijo: respuesta })
                    resolve()
                })
        })

        try {
            get_cuentas.then(() => {
                get_centroscosto.then(() => {
                    get_sucursales.then(() => {
                        //get_clientes.then(() => {
                            get_tiposdocumento.then(() => {
                                get_tipodocumentosBancarios.then(() => {
                                    get_prefijos.then(() => {
                                        resolve()
                                    })
                                })
                            })
                        //})
                    })
                })
            })
        } catch (error) {
            reject(error)
        }
    })
}

export function init(domElement, callbacks) {
    if (!(domElement instanceof HTMLElement) || domElement == null || domElement == undefined) { 
        console.error(`No se ha entregado un parámetro válido, los tipos esperados son HTMLElement, se ha entregado ${typeof domElement}`, domElement)
        return
    }

    cargar_data(domElement)
        .then( () => {
            let contexto = {
                idcomprobante: domElement.getAttribute('idcomprobante'),
                dev: domElement.getAttribute('dev'),
                callbacks: callbacks
            }
            const grilla = render(
                <Provider store={store} >
                    <Grilla contexto={contexto} componente={0} />
                </Provider>,
                domElement
            )
        }).catch( (error) => {
            console.error('Ha sucedido un error mientras se intentaban cargar los datos', error)
        })
}
