import { combineReducers } from 'redux'
import data from './red_grilla'
import data_global from './red_data_global'


const reducers = combineReducers({
    data,
    data_global
    //Se agregan si exiten más reducers
})

export default reducers
