import {
    SET_DATA_CUENTA,
    SET_DATA_CENTRO_COSTO,
    SET_DATA_SUCURSALES,
    SET_DATA_CLIENTES,
    SET_DATA_TIPO_DOCUMENTO,
    SET_DATA_TIPO_DOCUMENTO_BANCARIO,
    SET_DATA_PREFIJO
} from '../comprobante.jsx'

const estado_inicial =
    {
        data: []
    }

const data_global = (state = estado_inicial, action) => {
    switch (action.type) {
        case SET_DATA_CUENTA:
            return Object.assign({}, state, { data: action.data_cuenta })
            break
        case SET_DATA_CENTRO_COSTO:
            return Object.assign({}, state, { data: action.data_centrocosto })
            break
        case SET_DATA_SUCURSALES:
            return Object.assign({}, state, { data: action.data_sucursal })
            break
        case SET_DATA_CLIENTES:
            return Object.assign({}, state, { data: action.data_cliente })
            break
        case SET_DATA_TIPO_DOCUMENTO:
            return Object.assign({}, state, { data: action.data_tipo_documento })
            break
        case SET_DATA_TIPO_DOCUMENTO_BANCARIO:
            return Object.assign({}, state, { data: action.data_tipo_documento_bancario })
            break
        case SET_DATA_PREFIJO:
            return Object.assign({}, state, { data: action.data_prefijo })
            break
        default:
            return state
            break
    }
}

export default data_global
