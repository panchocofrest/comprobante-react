import {
    GET_DATA_GRID,
} from '../actions/act_grilla'

const estado_inicial =
    {
        items: []
    }

const grid = (state = estado_inicial, action) => {
    switch (action.type) {
        case GET_DATA_GRID:
            return Object.assign({}, state, { items: action.data_grid })
            break
        default:
            return state
            break
    }
}

export default grid
