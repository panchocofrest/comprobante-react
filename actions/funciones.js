//valida campos de entrada.
export function valida_campos(data_axios) {
        let url_axios       = data_axios.url
        let source          = data_axios.source
        //VALIDACIÓN URL              
        if( url_axios == undefined)
        {
            url_axios = false
            console.log('No se puede leer URL en archivo de configuración.')
        }
        const obj_retorno =
        [{
            url_config  :url_axios,
            source      :source 
        }]

    return obj_retorno
}

export function next_focus(element) {
    let brothers = $("#initBody").find('.nivel-li[tabindex=0]')

    let ids = []

    brothers.each(function (key, value) {
        ids.push(value.id)
    })

    let endIds = true
    let id = element.id
    let elementId = element.id

    if (element.tagName == 'INPUT' || element.tagName == 'DIV') {
        id = element.parentElement.id
    }

    if ($('#' + id).parent()[0] === undefined)
        return false

    let position = ids.indexOf(id)
    let nextPosition = parseInt(position) + 1
    let nextId
    if (nextPosition <= (ids.length - 1)) {
      endIds = false
      nextId = ids[nextPosition]
    }

    document.getElementById(elementId).blur()

    if (!endIds)
        document.getElementById(nextId).focus()

    return false
}

export function expanded_row(row) {
    let padre = $(row).parent()

    $(row).css({ 'height': 'auto' })

    $(padre).addClass('activo')
    var labelCelda = $(row).parent().find('.label')[0]
    $(labelCelda).removeClass('collapsed')
    $(labelCelda).addClass('expanded')

    $(row).find('.nivel-li').each(function (i, dat) {
        if (!$(dat).hasClass('borrar'))
            $(dat).attr('tabindex', '0')
    })
}

export function collapsed_row(row) {
    let padre = $(row).parent()
    $(row).css({ 'height': '' })
    
    $(padre).removeClass('activo')
    let labelCelda = $(row).parent().find('.label')[0]
    $(labelCelda).removeClass('expanded')
    $(labelCelda).addClass('collapsed')

    $(row).find('.nivel-li').each(function (i, dat) {
        if (!$(dat).hasClass('borrar'))
            $(dat).attr('tabindex', '-1')
    })
}

export function collapsed_all(elementId) {
    $('#grid').find('.level-1').each(function (position, level1) {
        if ($(level1).hasClass('activo') && $(level1)[0].id !== elementId) {
            $(level1).find('.level-2').each(function (i, row) {
                collapsed_row(row)
            })
        }
    })
}

export function block_celda_monto(llave, value, label) {
    if (llave === 'Debe') {
        if (parseInt(value) > 0) {
            if (label.getAttribute('tabindex') !== '-1') {
                document.getElementById(label.id).nextSibling.setAttribute('tabindex', '-1')
            }
        }
    }
    else if (llave === 'Haber') {
        if (parseInt(value) > 0) {
            if (label.getAttribute('tabindex') !== '-1') {
                document.getElementById(label.id).previousSibling.setAttribute('tabindex', '-1')
            }
        }
    }
}

//Dar formato a numeros
export function formatear_numero(num) {
    var formatNumber = {
        separador: ".", // separador para los miles
        sepDecimal: ',', // separador para los decimales
        formatear: function (num) {
            let monto = Math.round(num * 100) / 100
            monto += ''
            var splitStr = monto.split('.')
            var splitLeft = splitStr[0]
            var splitRight = splitStr.length > 1 ? this.sepDecimal + splitStr[1] : ''
            var regx = /(\d+)(\d{3})/
            while (regx.test(splitLeft)) {
                splitLeft = splitLeft.replace(regx, '$1' + this.separador + '$2')
            }
            return this.simbol + splitLeft + splitRight
        },
        new: function (num, simbol) {
            this.simbol = simbol || ''
            return this.formatear(num)
        }
    }

    return formatNumber.new(num)
}

export function event_fire(el, etype) {
    if (el.fireEvent) {
        el.fireEvent('on' + etype);
    } else {
        var evObj = document.createEvent('Events');
        evObj.initEvent(etype, true, false);
        el.dispatchEvent(evObj);
    }
}