export const objectAssign = require('object-assign');
// Obtiene ruta de config local:
//
// Se debe agregar un a carpeta "config_react" en la raíz del proyecto donde se integre notificaciones.
// Dentro de esta carpeta se debe crear un archivo "config_adjuntos" en el cual tiene la siguiente info:
// {
//   "config": "http:// -- SERVER -- /adjuntos.react/config.json"  // 
// }
//
export function url_config() {
    let ruta = window.location.origin + '/' + window.location.pathname.split('/')[1]
    if (ruta.substring(ruta.length - 1, ruta.length) === '/')
        ruta = ruta.substring(0, ruta.length - 1)

    let url_config = ruta + '/config_react/config_comprobante.json'

    return url_config
}

export function cabeceras_token(token, tipo, data) {
    let cabeceras = new Headers()
    cabeceras.append('Accept', 'application/json')
    cabeceras.append('utn', window.name);

    if (token)
        cabeceras.append("token", encodeURIComponent(token))

    let init = {
        method: tipo,
        headers: cabeceras,
        mode: 'cors',
        cache: 'default',
        body: JSON.stringify(data)
    }
    
    return init
}

export function cabecera_token_mvc(token, tipo, body, contentType) {
    let cabecera = {
        method: tipo,
        headers: {
            token: encodeURIComponent(token),
            "Content-Type": "application/json",
            "utn": window.name
        },
        cache: 'default',
        body: JSON.stringify(body),
        credentials: 'same-origin'
    }

    if (contentType)
        cabecera.headers = objectAssign(cabecera.headers, { 'Content-Type': contentType })

    return cabecera
}

export function ruteador_cabecera(token, tipo, body, contentType, dev) {
    let cabecera = {}
    if (dev) {
        cabecera = cabeceras_token(token, tipo, body)
    }
    else {
        cabecera = cabecera_token_mvc(token, tipo, body, contentType)
    }
    return cabecera;
}