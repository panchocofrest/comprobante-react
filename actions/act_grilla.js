export const GET_DATA_GRID = 'GET_DATA_GRID'

export function get_datos(api, source, data_default) {
    //Llamada a data de API
    return (dispatch, getState) => {
        let obtenerDataComprobante = localStorage.getItem("movimientoContable")
        if (obtenerDataComprobante !== null) {
            dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(obtenerDataComprobante) })
        }
        else {
            if (!api) {
                localStorage.setItem("movimientoContable", JSON.stringify(data_default))
                dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(localStorage.getItem("movimientoContable")) })
            }
            else {
                fetch(api, source)
                    .then((response) => {
                        document.getElementById("grillacomprobantereact").dataset.status = response.status
                        return response.json()
                    })
                    .then((data) => {
                        localStorage.setItem("movimientoContable", JSON.stringify(data.items))
                        dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(localStorage.getItem("movimientoContable")) })
                    })
            }
        }
    }
};

export function add_movimiento(movimiento, idItem, esDebito) {
    return (dispatch, getState) => {
        let data_actual = getState().data
        data_actual.items.forEach(function (element) {
            if (element.Id === idItem) {
                movimiento.CuentaId = element.CuentaId

                if (esDebito != null)
                    movimiento.EsDebito = esDebito

                Object.keys(movimiento).forEach(function (key, index) {
                    if (key == 'Id') {
                        movimiento.Id = movimiento[key]
                    }
                    else if (key == 'Remove') {
                        movimiento.Remove = movimiento[key]
                    }
                    else if (key == 'Accion') {
                        movimiento.Accion = movimiento[key]
                    }
                    else if (key == 'CuentaId') {
                        movimiento.CuentaId = movimiento[key]
                    }
                    else {

                        if (typeof movimiento[key] == 'string') {
                            if (key == 'Debe' || key == 'Haber') {
                                movimiento[key] = 0
                            }
                            else {
                                movimiento[key] = null
                            }
                        }

                    }
                });

                element.Movimientos[element.Tipo].push(movimiento)
            }
        }, this)

        let getComprobante = localStorage.getItem("movimientoContable")
        if (getComprobante !== null) {
            localStorage.removeItem("movimientoContable")
        }
        localStorage.setItem("movimientoContable", JSON.stringify(data_actual.items))

        dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(localStorage.getItem("movimientoContable")) })
    }
};

export function add_item(item) {
    return (dispatch, getState) => {
        coreAddItem(getState, item)
        dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(localStorage.getItem("movimientoContable")) })
    }
};

export function coreAddItem(getState, item) {
    let data_actual = getState().data

    item.Id = parseInt('1' + String(Math.random()).split('.')[1])
    item.Num = data_actual.items.filter(item => item.Accion != 'delete').length + 1

    data_actual.items.push(item)

    let getComprobante = localStorage.getItem("movimientoContable")
    if (getComprobante !== null) {
        localStorage.removeItem("movimientoContable")
    }
    localStorage.setItem("movimientoContable", JSON.stringify(data_actual.items))
}

export function update_field(itemId, movimientoId, key, dato, type, tipoControl, accion, esDatoRequerido, itemDefault) {
    return (dispatch, getState) => {
        let data_actual = getState().data
        let label = (typeof dato === 'object') ? dato.Detalle : dato
        let tiposMovimiento = {
            aux: 'aux',
            ban: 'ban'
        }

        var cargar = function (registro) {
            if (tipoControl === 'Autocomplete') {
                registro[key + 'Id'] = dato.Id
            }
        }

        /**
         * Paso 1: Completar valor contrario
         * obtener sumatoria de montos para movimientos contables (movimientos externos)
         */
        let itemDebe = 0
        let itemHaber = 0

        data_actual.items.forEach(function (element) {
            if (element.Accion != "delete") {
                itemDebe += element.Debe
                itemHaber += element.Haber
            }
        }, this)
        /** Fin paso 1 */

        data_actual.items.forEach(function (element) {
            if (element.Id === itemId) {

                if (movimientoId === null) {
                    if (key === 'Debe' || key === 'Haber') {
                        let valor = parseFloat(label.toString().replace(/\,/g, '.'))
                        let total = (isNaN(valor)) ? 0 : Math.round(valor * 100) / 100
                        label = null;

                        if (total > 0) {
                            label = total
                            element.EsDebito = (key == 'Debe') ? true : false
                        }
                        else {
                            element.EsDebito = null
                        }
                    }
                    else {
                        if (type === "int") {
                            label = parseInt(label)
                        }
                    }

                    element[key] = label

                    cargar(element)

                    if (tipoControl === 'Autocomplete' && key === 'Cuenta') {

                        element["RequiereCentrodeCosto"] = dato.RequiereCentrodeCosto
                        element["RequiereSucursal"] = dato.RequiereSucursal
                        element["TipoContraparte"] = dato.TipoContraparte

                        var cargaTipoCuenta = function () {
                            let tipoCuenta = dato.TipoCuenta
                            element['Tipo'] = tipoCuenta

                            if (tipoCuenta !== null)
                                return true

                            return false
                        }

                        if (accion === 'insert') {
                            /** Elimina movimientos existentes */
                            if (element.Tipo !== null) {
                                if (element.Movimientos[element.Tipo].length) {
                                    if (element.CuentaId !== element.Movimientos[element.Tipo][0].CuentaId) {
                                        element.Movimientos[element.Tipo].forEach(function (movimiento) {
                                            if (element.CuentaId !== movimiento.CuentaId)
                                                movimiento.Accion = 'delete'
                                        })
                                    }
                                }
                            }

                            if (cargaTipoCuenta()) {
                                // si existen movimiento y el CuentaId es distinto a la seleccionada, se eliminan los movimientos
                                if (element.Movimientos[element.Tipo] !== null) {
                                    if (element.Movimientos[element.Tipo][0] !== undefined)
                                        if (element.CuentaId !== element.Movimientos[element.Tipo][0].CuentaId)
                                            element.Movimientos[element.Tipo] = []
                                }
                                else if (element.Movimientos[element.Tipo] === null) {
                                    element.Movimientos[element.Tipo] = []
                                }
                            }
                        }
                        else {
                            // accion = 'update'
                            if (element.Tipo !== null) {
                                if (element.Movimientos[element.Tipo].length) {
                                    element.Movimientos[element.Tipo].forEach(function (movimiento) {
                                        if (element.CuentaId !== movimiento.CuentaId)
                                            movimiento.Accion = 'delete'
                                    })
                                }
                            }

                            if (cargaTipoCuenta()) {
                                if (!element.Movimientos[element.Tipo].length)
                                    element.Movimientos[element.Tipo] = []
                            }
                        }

                        /** 
                         * Paso 2: Completar valor contrario
                         * Asignar a movimiento contable la diferencia del monto al contrario
                         * Solo en caso de no ser cuenta AUX ni BAN (element['Tipo'] == null)
                         */
                        if (!cargaTipoCuenta()) {
                            if (parseFloat(itemDebe) > parseFloat(itemHaber)) {
                                if (!(parseFloat(element.Debe) > 0)) {
                                    element.Haber = parseFloat(itemDebe) - parseFloat(itemHaber)
                                    element.EsDebito = false
                                }
                            }
                            else if (parseFloat(itemDebe) < parseFloat(itemHaber)) {
                                if (!(parseFloat(element.Haber) > 0)) {
                                    element.Debe = parseFloat(itemHaber) - parseFloat(itemDebe)
                                    element.EsDebito = true
                                }
                            }
                        }
                        /** Fin paso 2 */

                    }
                }
                else {

                    let sumDebe = 0
                    let sumHaber = 0

                    /**
                     * Paso 3: Completar valor contrario
                     * obtener sumatoria de montos para movimientos auxiliares o bancarios (movimientos internos)
                     */
                    let movimientoDebe = 0
                    let movimientoHaber = 0

                    element.Movimientos[element.Tipo].forEach(function (movimiento) {
                        if (movimiento.Accion != "delete") {
                            movimientoDebe += parseFloat(movimiento.Debe)
                            movimientoHaber += parseFloat(movimiento.Haber)
                        }
                    });
                    /** Fin paso 3 */

                    element.Movimientos[element.Tipo].forEach(function (movimiento) {

                        if (movimiento.Id === movimientoId) {
                            if (key === 'Debe' || key === 'Haber') {
                                let valor = parseFloat(label.toString().replace(/\,/g, '.'))
                                let total = (isNaN(valor)) ? 0 : Math.round(valor * 100) / 100
                                label = null;
                                if (total > 0) {
                                    label = total
                                    element.EsDebito = (key == 'Debe') ? true : false
                                }
                            }
                            else if (key === 'Documento') {
                                // Documento tipo factura
                                //if (dato.Id !== 1) {
                                let tipodocumentoforprefijo = document.getElementById('grillacomprobantereact').getAttribute('tipodocumentoforprefijo')
                                let arraytipodocumento = (tipodocumentoforprefijo != undefined) ? tipodocumentoforprefijo.split('|') : [1]

                                if (arraytipodocumento.indexOf(dato.Id.toString()) == -1) {
                                    movimiento.Prefijo = ""
                                    movimiento.PrefijoId = 0
                                }
                            }
                            else {
                                if (type === "int") {
                                    label = parseInt(label)
                                }
                            }

                            movimiento[key] = label

                            if (esDatoRequerido) {
                                movimiento.tieneDatoRequerido = esDatoRequerido
                                /**
                                 * Paso 4: Completar valor contrario
                                 * obtener sumatoria de movimientos externos
                                 * evaluar diferencia en movimientos externos para asignar diferencia al movimiento con menor sumatoria
                                 * actualizar valor del movimiento interno en base a diferencia de movimientos externos y movimientos internos
                                 * actualizar valor del movimiento externo padre en base a sumatoria de los movimientos internos hijos
                                 * identificar y asignar esDebito
                                 */
                                if (parseFloat(itemDebe) > parseFloat(itemHaber)) {
                                    if (!(movimiento.Haber > 0) && (element.EsDebito === null || element.EsDebito === false)) {
                                        movimiento.Haber = ((parseFloat(itemDebe) - parseFloat(itemHaber)) + parseFloat(movimientoHaber)) - parseFloat(movimientoHaber)
                                        element.Haber = parseFloat(movimientoHaber) + movimiento.Haber
                                        element.EsDebito = false
                                    }
                                }
                                else if (parseFloat(itemHaber) > parseFloat(itemDebe) > 0) {
                                    if (!(movimiento.Debe > 0) && (element.EsDebito === null || element.EsDebito === true)) {
                                        movimiento.Debe = ((parseFloat(itemHaber) - parseFloat(itemDebe)) + parseFloat(movimientoDebe)) - parseFloat(movimientoDebe)
                                        element.Debe = parseFloat(movimientoDebe) + movimiento.Debe
                                        element.EsDebito = true
                                    }
                                }
                                /** Fin paso 4 */
                            }
                            cargar(movimiento)
                        }

                        sumDebe += parseFloat((isNaN(parseFloat(movimiento.Debe))) ? 0 : movimiento.Debe)
                        sumHaber += parseFloat((isNaN(parseFloat(movimiento.Haber))) ? 0 : movimiento.Haber)

                    }, this)

                    if (key === 'Debe' || key === 'Haber') {

                        element.EsDebito = null
                        element.Debe = sumDebe
                        element.Haber = sumHaber

                        if (sumDebe > 0 && sumHaber == 0) {
                            element.EsDebito = true
                        }

                        if (sumDebe == 0 && sumHaber > 0) {
                            element.EsDebito = false
                        }

                    }

                    element.Movimientos[element.Tipo].forEach(function (movimiento) {
                        movimiento.EsDebito = element.EsDebito
                        movimiento.Monto = (element.EsDebito == null) ? 0 : (element.EsDebito) ? sumDebe : sumHaber
                    }, this)

                }

                if (element.Tipo != null && element.Movimientos[element.Tipo].length <= 0) {
                    element.Debe = 0
                    element.Haber = 0
                }

            }
        }, this)

        let getComprobante = localStorage.getItem("movimientoContable")
        if (getComprobante !== null) {
            localStorage.removeItem("movimientoContable")
        }

        localStorage.setItem("movimientoContable", JSON.stringify(data_actual.items))

        /**
         * Agrega nuevo item
         * al actualizar el dato de cuenta
         */
        if (tipoControl === 'Autocomplete' && key === 'Cuenta') {
            if (dato.Id !== undefined && dato.Id !== null && dato.Id > 0) {
                coreAddItem(getState, itemDefault)
            }
        }

        dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(localStorage.getItem("movimientoContable")) })
    }
};

export function delete_row(itemId, movimientoId) {
    return (dispatch, getState) => {
        let posicion = 0
        let data_actual = getState().data

        data_actual.items.forEach(function (element) {
            posicion++

            if (element['Accion'] !== 'delete') {
                if (element.Id === itemId) {
                    if (movimientoId === null) {
                        posicion--
                        element['Accion'] = 'delete'
                        element['Num'] = -1

                        if (element.Movimientos !== null && element.Movimientos[element.Tipo] !== undefined) {
                            element.Movimientos[element.Tipo].forEach(function (movimiento) {
                                movimiento['Accion'] = 'delete'
                            }, this)
                        }
                    }
                    else {
                        let sumDebe = 0
                        let sumHaber = 0

                        element.Movimientos[element.Tipo].forEach(function (movimiento) {

                            if (movimiento.Id === movimientoId) {
                                movimiento['Accion'] = 'delete'
                                movimiento['Debe'] = 0
                                movimiento['Haber'] = 0
                            }

                            sumDebe += parseFloat((isNaN(parseFloat(movimiento.Debe))) ? 0 : movimiento.Debe)
                            sumHaber += parseFloat((isNaN(parseFloat(movimiento.Haber))) ? 0 : movimiento.Haber)

                        }, this)

                        element.Debe = sumDebe
                        element.Haber = sumHaber
                    }
                }
                else {
                    element['Num'] = posicion
                }
            }
            else
                posicion--

        }, this)

        let getComprobante = localStorage.getItem("movimientoContable")
        if (getComprobante !== null) {
            localStorage.removeItem("movimientoContable")
        }
        localStorage.setItem("movimientoContable", JSON.stringify(data_actual.items))

        dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(localStorage.getItem("movimientoContable")) })
    }
};

export function clean_data_for_save() {
    return (dispatch, getState) => {
        let posicion = 0
        let data_original = getState().data
        let data_items = []

        data_original.items.forEach(function (element) {
            if (element.CuentaId != 0) {
                data_items.push(element)
            }
        }, this)

        data_items.forEach(function (element) {
            if (element.Tipo != null) {
                let data_movimientos = []
                let debe = 0
                let haber = 0
                element.Movimientos[element.Tipo].forEach(function (movimiento) {
                    if (movimiento.tieneDatoRequerido) {
                        debe += movimiento.Debe
                        haber += movimiento.Haber
                        data_movimientos.push(movimiento)
                    }
                }, this)
                element.Movimientos[element.Tipo] = []
                element.Movimientos[element.Tipo] = data_movimientos
                element.Debe = debe
                element.Haber = haber
            }
        }, this)

        let getComprobante = localStorage.getItem("movimientoContable")
        if (getComprobante !== null) {
            localStorage.removeItem("movimientoContable")
        }
        localStorage.setItem("movimientoContable", JSON.stringify(data_items))

        dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(localStorage.getItem("movimientoContable")) })
    }
};

export function set_data_default(data_default) {
    return (dispatch, getState) => {
        let obtenerDataComprobante = localStorage.getItem("movimientoContable")
        if (obtenerDataComprobante !== null) {
            localStorage.removeItem("movimientoContable")
        }

        localStorage.setItem("movimientoContable", JSON.stringify(data_default))
        dispatch({ type: GET_DATA_GRID, data_grid: JSON.parse(localStorage.getItem("movimientoContable")) })
    }
};